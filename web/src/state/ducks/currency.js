import {convertCurrencyRates} from "@api/currency";

export const GET_CURRENCY_SUCCESS = "GET_CURRENCY_SUCCESS";
export const GET_CURRENCY_FAILURE = "GET_CURRENCY_FAILURE";
export const GET_CURRENCY_REQUEST = "GET_CURRENCY_REQUEST";

export const initialState = {
    isLoading: false,
    error: null,
    data: []
};

function getCurrencyRequest(payload) {
    return {
        type: GET_CURRENCY_REQUEST,
        payload
    }
}

function getCurrencySuccess(payload) {
    return {
        type: GET_CURRENCY_SUCCESS,
        payload
    }
}

function getCurrencyFailure(payload) {
    return {
        type: GET_CURRENCY_FAILURE,
        payload
    }
}

export function fetchConvertCurrencyRates(from, to, amount) {
    return function (dispatch) {
        dispatch(getCurrencyRequest());
        return convertCurrencyRates(from, to, amount)
            .then(response => {
                dispatch(getCurrencySuccess(response))
            }).catch(error => {
                dispatch(getCurrencyFailure(error.response.data))
            })
    }
}

export default (state = initialState, action) => {
    let { payload } = action;
    switch (action.type) {
        case GET_CURRENCY_SUCCESS:
            return {
                ...state,
                data: [...state.data, payload],
                isLoading: false
            };
        case GET_CURRENCY_FAILURE:
            return {
                ...state,
                isLoading: false,
                error: payload
            };
        case GET_CURRENCY_REQUEST:
            return {
                ...state,
                error: null,
                isLoading: true
            };
        default:
            return state
    }
}
