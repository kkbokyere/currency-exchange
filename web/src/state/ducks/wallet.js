import {getLatestCurrencyRates} from "@api/currency";

export const GET_LATEST_CURRENCY_SUCCESS = "GET_LATEST_CURRENCY_SUCCESS";
export const GET_LATEST_CURRENCY_FAILURE = "GET_LATEST_CURRENCY_FAILURE";
export const GET_LATEST_CURRENCY_REQUEST = "GET_LATEST_CURRENCY_REQUEST";

export const initialState = {
    isLoading: false,
    error: null,
    data: []
};

function getLatestCurrencyRequest(payload) {
    return {
        type: GET_LATEST_CURRENCY_REQUEST,
        payload
    }
}

function getLatestCurrencySuccess(payload) {
    return {
        type: GET_LATEST_CURRENCY_SUCCESS,
        payload
    }
}

function getLatestCurrencyFailure(payload) {
    return {
        type: GET_LATEST_CURRENCY_FAILURE,
        payload
    }
}

export function fetchLatestCurrencyRates(from, to, amount) {
    return function (dispatch) {
        dispatch(getLatestCurrencyRequest());
        return getLatestCurrencyRates(from, to, amount)
            .then(response => {
                dispatch(getLatestCurrencySuccess(response))
            }).catch(error => {
                dispatch(getLatestCurrencyFailure(error.response.data))
            })
    }
}

export default (state = initialState, action) => {
    let { payload } = action;
    switch (action.type) {
        case GET_LATEST_CURRENCY_SUCCESS:
            return {
                ...state,
                data: [...state.data, payload],
                isLoading: false
            };
        case GET_LATEST_CURRENCY_FAILURE:
            return {
                ...state,
                isLoading: false,
                error: payload
            };
        case GET_LATEST_CURRENCY_REQUEST:
            return {
                ...state,
                error: null,
                isLoading: true
            };
        default:
            return state
    }
}
