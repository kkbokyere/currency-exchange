import axios from 'axios'
export const API_URL = process.env.REACT_APP_API_URL;
export const API_KEY = process.env.REACT_APP_API_KEY;

export const getLatestCurrencyRates = (currency_codes = []) => {
    return axios.get(`${API_URL}/latest?apikey=${API_KEY}&symbols=${currency_codes}`)
        .then(response => {
            return response.data
        })
};

export const convertCurrencyRates = (from, to, amount) => {
    return axios.get(`${API_URL}/latest?apikey=${API_KEY}&from=${from}&to=${to}&amount=${amount}`)
        .then(response => {
            return response.data
        })
};
