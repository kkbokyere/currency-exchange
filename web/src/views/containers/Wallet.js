import { connect } from 'react-redux'
import List from "@components/List";
const mapStateToProps = ({ wallet }) => {
    return {
        wallet
    }
};

export default connect(
    mapStateToProps,
    null
)(List)
