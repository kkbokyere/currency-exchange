import { connect } from 'react-redux'
import List from "@components/List";
const mapStateToProps = ({ currency }) => {
    return {
        currency: currency.data
    }
};

export default connect(
    mapStateToProps,
    null
)(List)
