export const mockResponse = {
    "date": "2020-07-02 10:50:00+00",
    "current_rates": {
        "USD": 1.0,
        "PKR": 167.0
    },
    "converted_amount": 83500.0,
    "query": {
        "given_amount": 500.0,
        "from": "USD",
        "to": "PKR"
    }
};
